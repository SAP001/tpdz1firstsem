#include "number_convert.h"

int get_symbol_count(const char* symbols)
{
    int counter = 0;
    while (symbols[counter])
        counter++;

    return counter;
}

void rome_to_revers_digits(const char* roman_num, int* revers_rome_in_digit,const int count)
{
    for(int i = 0; i < count; i++)
    {
        revers_rome_in_digit[count - i - 1] = roman_to_arab_digit(roman_num[i]);
    }
}

bool roman_to_arabic(const char* roman_num, int *arabic_num)
{
    if(!is_roman_correct(roman_num))
        return false;

    int symbol_count = get_symbol_count(roman_num);
    int *revers_rome_in_digit = (int*)malloc(sizeof(int) * symbol_count);
    assert(revers_rome_in_digit != NULL);
    
    rome_to_revers_digits(roman_num, revers_rome_in_digit, symbol_count);

    *arabic_num = 0;
    int prev_value = 0, curr_value = 0;
    for(int i = 0; i < symbol_count; i++)
    {
        curr_value = revers_rome_in_digit[i];

        if(curr_value < prev_value)
            *arabic_num -= curr_value;
        else
            *arabic_num += curr_value;

        prev_value = curr_value;
    }

    free(revers_rome_in_digit);

    return true;
}