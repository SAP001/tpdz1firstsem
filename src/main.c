#include <string.h>
#include <malloc.h>
#include <assert.h>
#include "number_convert.h"

int main() {
    char roman_num[MAX_ROMAN_NUMBER_SIZE + 1];
    int arabic_num = 0;

    assert(scanf("%15s", roman_num) == ONE_ARGUMENTS_READ);

    if(roman_to_arabic(roman_num, &arabic_num))
        printf("%d\n", arabic_num);
    else
        printf("%s\n", "Not valid roman number!");

    return SUCCESS;
}
