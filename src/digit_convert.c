#include "digit_convert.h"

int roman_to_arab_digit(const char roman_digit)
{
    switch (roman_digit) {
        case 'I':
            return ARAB_I;
        case 'V':
            return ARAB_V;
        case 'X':
            return ARAB_X;
        case 'L':
            return ARAB_L;
        case 'C':
            return ARAB_C;
        case 'D':
            return ARAB_D;
        case 'M':
            return ARAB_M;
        default:
            return DOES_NOT_EXIST;
    }
}

char arab_to_roman_digit(const int arab_digit)
{
    switch (arab_digit) {
        case ARAB_I:
            return 'I';
        case ARAB_V:
            return 'V';
        case ARAB_X:
            return 'X';
        case ARAB_L:
            return 'L';
        case ARAB_C:
            return 'C';
        case ARAB_D:
            return 'D';
        case ARAB_M:
            return 'M';
        default:
            return NOT_ROMAN;
    }
}