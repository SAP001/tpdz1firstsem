#include "validator.h"

const char* rome_symbol = "IVXLCDM";
const char* roman_repeat_digits = "IXCM";

bool is_characters_valid(const char* roman_num)
{
    int counter = 0;
    while (roman_num[counter])
    {
        if(!strchr(rome_symbol, roman_num[counter++]))
            return false;
    }
    return true;
}

bool is_composite_number(const char first, const char second)
{
    switch (second) {
        case 'V':
        case 'X': return first == 'I';

        case 'L':
        case 'C': return first == 'X';

        case 'D':
        case 'M': return first == 'C';

        default: return false;
    }
}

bool is_sequence_valid(const char* roman_num) 
{
    int last_arab_digit = MAX_ROMAN_DIGIT + 1;
    int curr_arab_digit = 0;
    int next_arab_digit = 0;
    char next_roman_digit = '\0';
    //Некоторые арабские цифры могут повторятся
    int repeat_counter = 0;
    int counter = -1;

    while (roman_num[++counter]) {
        curr_arab_digit = roman_to_arab_digit(roman_num[counter]);
        next_roman_digit = roman_num[counter + 1];
        next_arab_digit = roman_to_arab_digit(next_roman_digit);

        //Определяем составное ли число
        if (!is_composite_number(roman_num[counter], roman_num[counter + 1]))
        {
            //контроль за кол-вом повторений
            bool is_repeated_digit = strchr(roman_repeat_digits, roman_num[counter]);
            if (!is_repeated_digit && last_arab_digit == curr_arab_digit ||
                    is_repeated_digit && last_arab_digit == curr_arab_digit && repeat_counter == 3)
                return false;

            //не должно быть вычитание и добавление одного и того же числа
            if(arab_to_roman_digit(last_arab_digit + curr_arab_digit) != NOT_ROMAN)
                return false;

            //если число отличное от предыдущего счетчик повторений сбрасывается
            else if (curr_arab_digit != last_arab_digit)
                repeat_counter = 1;
            else
                repeat_counter++;
        } else {
            //В составых вычитаем из правого левое (IX = 9)
            curr_arab_digit = next_arab_digit -  curr_arab_digit;

            //одинаковые состовные числа повторяться не могут
            if (last_arab_digit == curr_arab_digit) return false;

            //после VLD не может стоять состваное число, влюч. эту же римскую цифру
            if (last_arab_digit ==  next_arab_digit &&
                    !strchr(roman_repeat_digits, next_roman_digit))
                return false;

            //поскольку число составное счетчик повторений обнуляется
            repeat_counter = 0;

            //для составного число нужно еще раз подвинуть counter
            counter++;
        }

        if (last_arab_digit < curr_arab_digit)
            return false;

        last_arab_digit = curr_arab_digit;
    }
    return true;
}

bool is_roman_correct(const char* roman_num)
{
    bool is_correct;
    is_correct = is_characters_valid(roman_num);
    is_correct = is_correct && is_sequence_valid(roman_num);

    return is_correct;
}