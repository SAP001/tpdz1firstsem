#include "gtest/gtest.h"

extern "C" {
#include "digit_convert.h"
}


TEST (convert_digit, roman_to_arab) 
{
  EXPECT_EQ (5, roman_to_arab_digit('V'));
  EXPECT_EQ (500, roman_to_arab_digit('D'));
  EXPECT_EQ (-1, roman_to_arab_digit('v'));
  EXPECT_EQ (-1, roman_to_arab_digit('K'));
}

TEST (convert_digit, arab_to_roman) 
{
  EXPECT_EQ ('V', arab_to_roman_digit(5));
  EXPECT_EQ ('D', arab_to_roman_digit(500));
  EXPECT_EQ ('\0', arab_to_roman_digit(23));
  EXPECT_EQ ('\0', arab_to_roman_digit(-2));
}
