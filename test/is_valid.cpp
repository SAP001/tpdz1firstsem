#include "gtest/gtest.h"

extern "C" {
#include "validator.h"
}


TEST (is_valid, is_roman_digit) {
  EXPECT_EQ (true, is_characters_valid("V"));
  EXPECT_EQ (true, is_characters_valid("VIXCV"));
  EXPECT_EQ (true, is_characters_valid("MMDCXIIIII"));
  EXPECT_EQ (false, is_characters_valid("MMSD"));
  EXPECT_EQ (false, is_characters_valid("MDDVvIII"));
  EXPECT_EQ (false, is_characters_valid("M123M"));
}

TEST (is_valid, is_composite_num) {
  EXPECT_EQ (false, is_composite_number('V', 'I'));
  EXPECT_EQ (false, is_composite_number('X', 'V'));
  EXPECT_EQ (true, is_composite_number('I', 'V'));
  EXPECT_EQ (true, is_composite_number('X','C'));
  EXPECT_EQ (false, is_composite_number('X', 'M'));
  EXPECT_EQ (true, is_composite_number('C', 'M'));

  EXPECT_EQ (false, is_composite_number('D', 'C'));
  EXPECT_EQ (true, is_composite_number('C', 'D'));
  EXPECT_EQ (true, is_composite_number('I', 'X'));
  EXPECT_EQ (false, is_composite_number('M','I'));
  EXPECT_EQ (false, is_composite_number('I', 'x'));
  EXPECT_EQ (false, is_composite_number('C', 'K'));
}

TEST (is_valid, is_sequence_correct) {
  EXPECT_EQ (false, is_sequence_valid("MMDCXIIIII"));
  EXPECT_EQ (false, is_sequence_valid("VIIII"));
  EXPECT_EQ (true, is_sequence_valid("VIII"));
  EXPECT_EQ (true, is_sequence_valid("IX"));
  EXPECT_EQ (false, is_sequence_valid("IIX"));
  EXPECT_EQ (false, is_sequence_valid("VV"));
  EXPECT_EQ (false, is_sequence_valid("DD"));

  EXPECT_EQ (false, is_sequence_valid("XMMM"));
  EXPECT_EQ (false, is_sequence_valid("MXMM"));
  EXPECT_EQ (false, is_sequence_valid("MMXM"));
  EXPECT_EQ (true, is_sequence_valid("CXC"));
  EXPECT_EQ (false, is_sequence_valid("XCX"));
  EXPECT_EQ (false, is_sequence_valid("ICX"));
  EXPECT_EQ (true, is_sequence_valid("XCI"));

  EXPECT_EQ (true, is_sequence_valid("MMMDCCCIX"));
  EXPECT_EQ (false, is_sequence_valid("MMMDCCICX"));
  EXPECT_EQ (false, is_sequence_valid("MMMDCICCX"));
  EXPECT_EQ (false, is_sequence_valid("VX"));
  EXPECT_EQ (false, is_sequence_valid("DM"));
  EXPECT_EQ (false, is_sequence_valid("LC"));
  EXPECT_EQ (true, is_sequence_valid("MCCCI"));

}