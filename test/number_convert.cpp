#include "gtest/gtest.h"

extern "C" {
#include "number_convert.h"
}


TEST (number_convert, simbol_count) 
{
  EXPECT_EQ (4, get_symbol_count("1234"));
  EXPECT_EQ (2, get_symbol_count("23"));
  EXPECT_EQ (0, get_symbol_count(""));
  EXPECT_EQ (4, get_symbol_count("Aas2"));
}


TEST (number_convert, revers_digits) 
{
  int arab_num[16];
  rome_to_revers_digits("MMDCX" ,arab_num, 5);
  EXPECT_EQ (10,arab_num[0]);
  EXPECT_EQ (100,arab_num[1]);
  EXPECT_EQ (500,arab_num[2]);
  EXPECT_EQ (1000,arab_num[3]);
  EXPECT_EQ (1000,arab_num[4]);

  rome_to_revers_digits("VI" ,arab_num, 2);
  EXPECT_EQ (1,arab_num[0]);
  EXPECT_EQ (5,arab_num[1]);
}

TEST (number_convert, roman_to_arab_num) 
{
  int  arab_num = 0;
  
  EXPECT_EQ (true, roman_to_arabic("MXC", &arab_num));
  EXPECT_EQ (1090, arab_num);


  EXPECT_EQ (true, roman_to_arabic("MMMDCCCLXXXVIII", &arab_num));
  EXPECT_EQ (3888, arab_num);

  EXPECT_EQ (true, roman_to_arabic("CMXCIX", &arab_num));
  EXPECT_EQ (999, arab_num);

  EXPECT_EQ (false, roman_to_arabic("IIV", &arab_num));
  EXPECT_EQ (false, roman_to_arabic("XCX", &arab_num));
}
