#pragma once
#ifndef NUMBER_CONVERT_H
#define NUMBER_CONVERT_H

#include <malloc.h>
#include <stdbool.h>
#include <assert.h>
#include "validator.h"

int get_symbol_count(const char* symbols);
void rome_to_revers_digits(const char* roman_num, int* revers_rome_in_digit,const int count);
bool roman_to_arabic(const char* roman_num, int *arabic_num);

#endif
