#pragma once
#ifndef VALIDATOR_H
#define VALIDATOR_H

#include <string.h>
#include <stdbool.h>
#include "digit_convert.h"

bool is_characters_valid(const char* roman_num);
bool is_composite_number(const char first, const char second);
bool is_sequence_valid(const char* roman_num);
bool is_roman_correct(const char* roman_num);


#endif