#pragma once
#ifndef DIGIT_CONVERT_H
#define DIGIT_CONVERT_H

#include "config.h"

int roman_to_arab_digit(const char roman_digit);
char arab_to_roman_digit(const int arab_digit);

#endif