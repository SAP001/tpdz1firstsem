#pragma once
#ifndef CONFIG_H
#define CONFIG_H

#define ARAB_I 1
#define ARAB_V 5
#define ARAB_X 10
#define ARAB_L 50
#define ARAB_C 100
#define ARAB_D 500
#define ARAB_M 1000
#define DOES_NOT_EXIST -1
#define MAX_ROMAN_DIGIT 1000
#define MAX_ROMAN_NUMBER_SIZE 15 //MMMDCCCLXXXVIII
#define SUCCESS 0
#define READ_ERR -1
#define ONE_ARGUMENTS_READ 1
#define NOT_ROMAN '\0'


#endif